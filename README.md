Makabunchasquares (MKB)
=================

This Python script creates and (separately) saves versions of images that it crops into squares.

For images that are wider than they are tall, MKB crops equally from both sides.

For images taller than they are wide, MKB crops from the bottom.

Setup
-----

MKB only requires Pillow.

    $ pip install Pillow

Or:

    $ pip install -r REQUIREMENTS.txt

Usage
-----

Example command:

    $ python mkb /path/to/input.png

Example
-------

Example images:

1. The Isaac N. Reynolds House in Eaton Rapids, MI, courtesy of Kennethaw88.
[CC BY 4.0](http://creativecommons.org/licenses/by/4.0), via Wikimedia Commons.

2. Confucius statue on Chongming Island, Shanghai, courtsey of Mamin27.
[CC BY-SA 2.0](https://creativecommons.org/licenses/by-sa/2.0), via Wikimedia Commons.

### Before

![A rectangular image of a house](example.png)

### After

![A square image of a house](example-formatted.png)

Thanks
------

Some code was borrowed from [pypa](https://github.com/pypa/), and some from [mpolden](https://github.com/mpolden).

Thank you for your help!
