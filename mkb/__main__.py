import os
import sys
import re
import glob
import getopt
import shutil
from PIL import Image

"""
This python script creates and (separately) saves versions of images that it
crops into squares.
"""

def __usage__(error=None):
    """
    Read the help file and print what is in the "Usage" section, and then exit.
    """
    with open('README.md', 'r') as helpfile:
        # Drop all help text before and after the "Usage" section.
        recording = False
        help = []
        for line in helpfile:
            line = line.replace('\n', '')
            if line == "":
                continue
            #  When encountering the "Usage section, print it.
            if re.match("^Usage", line):
                recording = True
            #  Ignore text after the end of the "Usage" section.
            elif recording and len(help) > 3 and re.match("^---", line):
                recording = False
            if recording:
                help.append(line)
        help = help[:-1]
        print("\n".join(help))
        if error:
            print(error)
        sys.exit()

def recognize_image_by_filename(image_name):
    return os.path.splitext(image_name)[-1].lower() in ['.png', '.jpg', '.jpeg']

def resize(source, target, verbose=True):
    """
    Format an image as a square.
    :param source: The location of the source image.
    :param target: The location to save.
    :return: True if successful, else False.
    """
    if not recognize_image_by_filename(source):
        print("Ignoring", source, "because it's not an image.")
        return False
    if not os.path.isfile(source):
        raise Exception('Source file "{}" does not exist.'.format(source))
    if os.path.isfile(target):
        prompt = 'Target filename "{}" already exists. Overwrite? [y/N]\n'
        overwrite = input(prompt.format(target)).strip()
        if overwrite == "" or overwrite[0].lower() != 'y':
            print("Ignoring {}.".format(source))
            return False
    # Make a copy of the image.
    shutil.copyfile(source, target)
    # Edit the copy.
    im = Image.open(target)
    width, height = im.size
    new_width = min(im.size)
    new_height = min(im.size)
    if height != new_height:
        # Portrait mode:
        left = 0
        top = 0
        bottom = new_height
        right = width
    elif width != new_width:
        # Landscape mode:
        left = (width - new_width) / 2
        right = (width + new_width) / 2
        top = 0
        bottom = height
    else:
        print('Image "{}" is already a square.'.format(source))
        return True
    if verbose:
        print("width:", width, "=>", new_width)
        print("Height:", height, "=>", new_height)
        print("Left:", left)
        print("Right:", right)
        print("Top:", top)
        print("Bottom:", bottom)
    im.crop((left, top, right, bottom)).save(target)
    print('Saved a square version at "{}".'.format(target))
    return True

def main():
    try:
        # Example:
        # opts, args = getopt(sys.argv[1:], 'hdsp:c', ['help', 'dump', 'sale',
        #                                              'pct_off=', 'colors'])
        opts, args = getopt.getopt(sys.argv[1:], 'h', ['help'])
    except getopt.GetoptError as err:
        print("Error getting options:")
        __usage__(err)

    for opt, arg in opts:
        if opt in ('-h', '--help'):
            __usage__()
        else:
            print("opt:", opt)
            print("arg:", arg)

    sources = [source_arg for source_arg in sys.argv[1:] if source_arg[0] != '-']
    if sources == []:
        __usage__("Error: no arguments given.")

    for i, source in enumerate(sources):
        print("Processing source file \"{}\".".format(source))
        target_name = list(os.path.splitext(source))
        target_name[-2] = target_name[-2]+"-formatted".format(i+1)
        target_name = ''.join(target_name)
        resize(source, target_name)

if __name__ == "__main__":
    main()
